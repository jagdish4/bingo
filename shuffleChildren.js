var originalcards = [];
var used_number = [];


$.fn.shuffleCards = function() {
  $.each(this.get(), function(index, el) {
    var $el = $(el);
    var $find = $el.children();
    
    used_number = [];
    originalcards.map(function(item, i){
      
      random = generateRandom(0, 24);
      used_number.push(random);
      $find[i] = originalcards[random].html;
    })

    $el.empty();
    $find.appendTo($el);
  });
};

$.fn.shuffleCardsWithFreeSpace = function() {
  $.each(this.get(), function(index, el) {
    var $el = $(el);
    var $find = $el.children();
    
    used_number = [12];
    originalcards.map(function(item, i){
      var random = 12;
      if(i != 12){
        random = generateRandom(0, 24);
      }
      used_number.push(random);
      $find[i] = originalcards[random].html;
    })

    $el.empty();
    $find.appendTo($el);
  });
};

$.fn.shuffleCardsInColumn = function() {
  $.each(this.get(), function(index, el) {
    var $el = $(el);
    var $find = $el.children();
    
    used_number = [];
    originalcards.map(function(item, i){
      var random = generateRandom(0, 24);
      
      used_number.push(random);
      $find[i] = originalcards[random].html;
    })

    $el.empty();
    $find.appendTo($el);
  });
};

function saveAllCards(){
  originalcards = [];
  var clildrens = $('.shuffle').children();
  clildrens.each((i,element) => {
    
    var card = {
      html : element,
      index : i
    }
    originalcards.push(card);
  });
}

function generateRandom(min, max) {
  var num = Math.floor(Math.random() * (max - min + 1)) + min;
  return (used_number.includes(num)) ? generateRandom(min, max) : num;
}

var test = generateRandom(1, 20)

function generateCard(){
  if($('#freeSpace').is(':checked')){
    $(".shuffle").shuffleCardsWithFreeSpace();
  }else{
    $(".shuffle").shuffleCards();
  }
}